import 'dart:async';

void main(List<String> args) async {
  print("Fall For You by Secondhand Serenade");
  print("");
  print(await line());
  print(await line2());
  print(await line3());
  print(await line4());
}

Future<String> line() async {
  String lirik = "The best thing 'bout tonight that we're not fighting..";
  return await Future.delayed(Duration(seconds: 2), () => (lirik));
}

Future<String> line2() async {
  String lirik = "Could it be that we have been this way before?";
  return await Future.delayed(Duration(seconds: 5), () => (lirik));
}

Future<String> line3() async {
  String lirik = "I know you don't think that I am trying..";
  return await Future.delayed(Duration(seconds: 6), () => (lirik));
}

Future<String> line4() async {
  String lirik = "I know you wearing thin down to the core";
  return await Future.delayed(Duration(seconds: 6), () => (lirik));
}
